import { Configuration, OpenAIApi } from "openai";

// It's best practice to load the API key from an environment variable for security
const configuration = new Configuration({
  apiKey: "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",
});
const openai = new OpenAIApi(configuration);

export default async function (req, res) {
  // Added logging to inspect the incoming request
  console.log('Received request:', req.body);

  // If the API key is not configured, return an error
  if (!configuration.apiKey) {
    res.status(500).json({
      error: {
        message: "API key not configured, please follow instructions in README.md to set your API key.",
      }
    });
    return;
  }

// Extract character from the request body
const character = req.body.actor || ''; // Use 'actor' as per your received requests

// Check if the character is valid (non-empty after trimming whitespace)
if (character.trim().length === 0) {
  res.status(400).json({
    error: {
      message: "Please enter a valid character name",
    }
  });
  return;
}

  // Try to get restaurant suggestions
  try {
    const chatCompletion = await openai.createChatCompletion({
      model: "gpt-4",
      messages: [{
        role: "system",
        content: "The user wants movies or shows suggestions for an actors."
      }, {
        role: "user",
        content: `Suggest a movie or TV show for the following character: ${character}`
      }],
    });
    // Send the suggestion back in the response
    res.status(200).json({ result: chatCompletion.data.choices[0].message.content });
  } catch (error) {
    // Handle errors from the OpenAI API
    if (error.response) {
      console.error(error.response.status, error.response.data);
      res.status(error.response.status).json(error.response.data);
    } else {
      console.error(`Error with OpenAI API request: ${error.message}`);
      res.status(500).json({
        error: {
          message: 'An error occurred during your request. Please check the OpenAI server status and your network connection.',
        }
      });
    }
  }
}

function generatePrompt(actor) {
  return `Suggest a movie or TV show for the following actor/character

Character: ${actor}
Movies:`;
}
