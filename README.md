# App1 Web Application

This repository contains the infrastructure and application code for the App1 web application. The `main.tf` file is used to create an AWS instance with Terraform, which then gets configured with NPM, NGINX, Docker Engine, and user configurations for Docker. Docker is used to pull the `pranavatla/app1` image, and NGINX is configured as a reverse proxy.

## Prerequisites

Before you begin, ensure you have met the following requirements on your local machine:
* You have installed the latest version of [Terraform](https://www.terraform.io/downloads.html).
Below are already part of the `main.tf` file for the EC2 instance that is created.
* You have installed [Docker](https://docs.docker.com/get-docker/).
* You have installed [NPM](https://www.npmjs.com/get-npm) and [Node.js](https://nodejs.org/en/).
* You have an [AWS account](https://aws.amazon.com/) with the necessary permissions to create resources.

**IMPORTANT**
* Please replace the AWS credentials keys in the `main.tf` file and the Open API key in the `.env` file.

## Setting Up Your Environment

To set up the App1 web application environment, follow these steps:

1. Clone this repository to your local machine.
2. Navigate to the `.terraform` directory and initialize your Terraform workspace:
   ```bash
   terraform init
   ```
3. Apply the Terraform configuration to create the AWS resources:
   ```bash
   terraform apply
   ```
   Confirm the action by typing `yes` when prompted.

Wait for the terraform apply command to complete and utilize the output after 150 seconds (allow the docker image to be pulled, deployed and reverse proxy by NGINX)

## Deploying the App LOCALLY

Navigate to the app directory:
```bash
cd app
```

Install the Node.js dependencies:
```bash
npm install
```

Start the application in development mode:
```bash
npm run dev
```

## Docker Container

The Docker container can be run by following these steps:

Ensure Docker Engine is running on your system.
```bash
docker --version
```

Pull the Docker image from Docker Hub:
```bash
docker pull pranavatla/app1
```

Run the Docker container:
```bash
docker run -d -p 80:3000 pranavatla/app1
```
This command will start the container and bind the host's port 80 to the container's port 3000.
