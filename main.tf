provider "aws" {
  region     = "ap-south-1"
  access_key = INSERT YOUR AWS KEYS
  secret_key = INSERT YOUR AWS KEYS 
}

resource "aws_instance" "app1" {
  ami                    = "ami-03bb6d83c60fc5f7c"
  instance_type          = "t2.micro"
  key_name               = "Pranav"
  vpc_security_group_ids = ["sg-05d5516638a0e6f2d"]

  user_data = <<-EOF
                #!/bin/bash
                sudo apt update
                sudo apt install -y apt-transport-https ca-certificates curl software-properties-common nginx
                
                # Install Docker
                curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
                sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
                sudo apt update
                sudo apt install -y docker-ce
                sudo systemctl start docker
                sudo systemctl enable docker

                # Add the ubuntu user to the docker group
                sudo usermod -aG docker ubuntu

                # Pull the Docker image from DockerHub
                sudo -u ubuntu docker pull pranavatla/app1:latest
                
                # Start a Docker container from the pulled image
                sudo docker run -d --name app1_container -p 3000:3000 pranavatla/app1:latest

                # Configure Nginx as a reverse proxy
                sudo rm /etc/nginx/sites-enabled/default
                echo "server {
                  listen 80;
                  location / {
                    proxy_pass http://localhost:3000;
                  }
                }" | sudo tee /etc/nginx/sites-available/app1
                sudo ln -s /etc/nginx/sites-available/app1 /etc/nginx/sites-enabled/
                sudo systemctl restart nginx
              EOF

  tags = {
    Name = "APP1"
    Environment = "app1_testing"
  }
}

output "app1_ssh_command" {
  value = "Access EC2 at: ssh -o StrictHostKeyChecking=no -i /Users/pranav/Pranav.pem ubuntu@${aws_instance.app1.public_ip}"
}

output "app1_access_command" {
  value = "Wait for two minutes and Access Movie Recommender at http://${aws_instance.app1.public_ip}"
}